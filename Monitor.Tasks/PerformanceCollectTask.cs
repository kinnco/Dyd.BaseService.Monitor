﻿using BSF.Config;
using Monitor.Domain.PlatformManage.Dal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor.Tasks
{
    /// <summary>
    /// 性能采集统计任务
    /// 建议cron： 1 0/5 * * * ?
    /// 描述:用于集群采集信息的定时统计汇总到另外的冗余表。
    /// （Cpu,读写磁盘,IIs请求，内存等）
    /// </summary>
    public class PerformanceCollectTask : BSF.BaseService.TaskManager.BaseDllTask
    {
        public override void Run()
        {
            try
            {
                InitConfig();
                Do();
            }
            catch (Exception exception)
            {
                base.OpenOperator.Error("性能监控定时统计失败", exception);
            }
        }

        public void InitConfig()
        {
            BSFConfig.MonitorPlatformConnectionString = base.AppConfig["MonitorPlatformConnectionString"];
        }

        private void Do()
        {
            PerformanceCollectDal Dal = new PerformanceCollectDal();
            bool c = Dal.StatisServerMinitor();
            if (c)
            {
                base.OpenOperator.Log("性能监控定时统计成功");
            }
        }

        public override void TestRun()
        {
            /*测试环境下任务的配置信息需要手工填写,正式环境下需要配置在任务配置中心里面*/
            base.AppConfig = new BSF.BaseService.TaskManager.SystemRuntime.TaskAppConfigInfo();
            base.AppConfig.Add("MonitorPlatformConnectionString", "server=192.168.17.201;Initial Catalog=dyd_bs_monitor_platform_manage;User ID=sa;Password=Xx~!@#;");
            base.TestRun();
        }
    }
}
